import { lazy } from "react";
import paths from "router/paths";

const Home = lazy(() => import("pages/Home"));

const routes = [
  {
    path: paths.home,
    component: Home,
    exact: true,
  },
];

export default routes;
