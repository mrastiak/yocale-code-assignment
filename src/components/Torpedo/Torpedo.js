import React from "react";
import PropTypes from "prop-types";

function Torpedo({ data }) {
  return <div>{data.name}</div>;
}

Torpedo.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string.isRequired,
  }).isRequired,
};

Torpedo.defaultProps = {};

export default Torpedo;
