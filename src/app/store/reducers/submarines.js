import { createSlice } from "@reduxjs/toolkit";
import initialData from "data/submarines.json";
import {
  selectTorpedoes,
  updateTorpedoes as updateTorpedoList,
} from "app/store/reducers/torpedoes";

const initialState = {
  selectedSubmarine: null,
  submarines: initialData,
  loading: false,
};

export const submarineSlice = createSlice({
  name: "submarine",
  initialState,
  reducers: {
    setSelectedSubmarine: (state, action) => {
      state.selectedSubmarine = action.payload;
    },
    updateTorpedoes: (state, action) => {
      state.selectedSubmarine.equipments.torpedoes = action.payload;
    },
    updateSubmarines: (state) => {
      state.submarines = state.submarines.map((submarine) => {
        if (submarine.id === state.selectedSubmarine.id) {
          submarine = state.selectedSubmarine;
        }
        return submarine;
      });
    },
    setLoading: (state, action) => {
      state.loading = action.payload;
    },
  },
});

export const {
  setSelectedSubmarine,
  updateTorpedoes,
  updateSubmarines,
  setLoading,
} = submarineSlice.actions;

export const selectSubmarines = (state) => state.submarines.submarines;
export const selectSelectedSubmarine = (state) =>
  state.submarines.selectedSubmarine;
export const selectSelectedSubmarineTorpedoes = (state) =>
  state.submarines.selectedSubmarine?.equipments.torpedoes;
export const selectLoading = (state) => state.submarines.loading;

export const loadTorpedo = () => async (dispatch, getState) => {
  const load = new Promise((resolve) => {
    setTimeout(() => {
      const [loadingTorpedo, ...rest] = selectTorpedoes(getState());
      const currentTorpedoes = [
        ...selectSelectedSubmarineTorpedoes(getState()),
      ];
      dispatch(updateTorpedoes([...currentTorpedoes, loadingTorpedo]));
      dispatch(updateSubmarines());
      dispatch(updateTorpedoList(rest));
      resolve();
    }, 1000);
  });
  dispatch(setLoading(true));
  await load;
  dispatch(setLoading(false));
};

export const unloadTorpedo = () => async (dispatch, getState) => {
  const unload = new Promise((resolve) => {
    setTimeout(() => {
      const unassignedTorpedoes = selectTorpedoes(getState());
      const currentTorpedoes = [
        ...selectSelectedSubmarineTorpedoes(getState()),
      ];
      const unloadingTorpedo = currentTorpedoes.pop();
      dispatch(updateTorpedoes(currentTorpedoes));
      dispatch(updateSubmarines());
      dispatch(updateTorpedoList([...unassignedTorpedoes, unloadingTorpedo]));
      resolve();
    }, 1000);
  });
  dispatch(setLoading(true));
  await unload;
  dispatch(setLoading(false));
};

export default submarineSlice.reducer;
