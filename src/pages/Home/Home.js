import React, { useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  setSelectedSubmarine,
  loadTorpedo,
  unloadTorpedo,
  selectSubmarines,
  selectSelectedSubmarine,
  selectSelectedSubmarineTorpedoes,
  selectLoading,
} from "app/store/reducers/submarines";
import { selectTorpedoes } from "app/store/reducers/torpedoes";
import cx from "classnames";
import SubmarineList from "components/SubmarineList";
import TorpedoList from "components/TorpedoList";
import Actions from "components/Actions";
import s from "./Home.module.css";

function Home() {
  const submarines = useSelector(selectSubmarines);
  const torpedoes = useSelector(selectTorpedoes);
  const dispatch = useDispatch();
  const selectedSubmarine = useSelector(selectSelectedSubmarine);
  const assignedTorpedoes = useSelector(selectSelectedSubmarineTorpedoes);
  const loading = useSelector(selectLoading);

  const handleLoad = useCallback(() => {
    if (
      !selectedSubmarine ||
      selectedSubmarine.equipments.torpedoes.length >= 3 ||
      !torpedoes.length
    ) {
      return;
    }
    dispatch(loadTorpedo());
  }, [selectedSubmarine, torpedoes, dispatch]);

  const handleUnload = useCallback(() => {
    if (
      !selectedSubmarine ||
      selectedSubmarine.equipments.torpedoes.length <= 0
    ) {
      return;
    }

    dispatch(unloadTorpedo());
  }, [selectedSubmarine, dispatch]);

  const handleSelect = useCallback(
    (submarine) => {
      !loading && dispatch(setSelectedSubmarine(submarine));
    },
    [dispatch, loading]
  );

  return (
    <main className={s.container}>
      <SubmarineList
        className={cx(s.box, s.submarines)}
        data={submarines}
        onSelect={handleSelect}
        selected={selectedSubmarine}
      />
      <TorpedoList
        className={cx(s.box, s.torpedoes)}
        data={assignedTorpedoes}
      />
      <TorpedoList
        className={cx(s.box, s.unassignedTorpedoes)}
        data={torpedoes}
      />
      <Actions
        className={cx(s.box, s.actions)}
        onLoad={handleLoad}
        onUnload={handleUnload}
        selectedSubmarine={selectedSubmarine}
        loading={loading}
      />
    </main>
  );
}

export default Home;
