import React from "react";
import PropTypes from "prop-types";
import Torpedo from "components/Torpedo";

function TorpedoList({ className, data }) {
  return (
    <section className={className}>
      {data.map((torpedo) => (
        <Torpedo key={torpedo.id} data={torpedo} />
      ))}
    </section>
  );
}

TorpedoList.propTypes = {
  className: PropTypes.string,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    })
  ),
};

TorpedoList.defaultProps = {
  className: "",
  data: [],
};

export default React.memo(TorpedoList);
