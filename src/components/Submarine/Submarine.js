import React from "react";
import PropTypes from "prop-types";
import s from "./Submarine.module.css";

function Submarine({ data, onSelect, selected }) {
  return (
    <div
      className={selected ? s.selected : undefined}
      onClick={() => onSelect(data)}
    >
      {data.name}
    </div>
  );
}

Submarine.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.string.isRequired,
  }).isRequired,
  onSelect: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired,
};

Submarine.defaultProps = {};

export default Submarine;
