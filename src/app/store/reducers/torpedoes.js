import { createSlice } from "@reduxjs/toolkit";
import initialData from "data/torpedoes.json";

const initialState = initialData;

export const torpedoSlice = createSlice({
  name: "torpedo",
  initialState,
  reducers: {
    updateTorpedoes: (state, action) => {
      return [...action.payload];
    },
  },
});

export const { updateTorpedoes } = torpedoSlice.actions;

export const selectTorpedoes = (state) => state.torpedoes;

export default torpedoSlice.reducer;
