# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 0.1.0 (2021-05-05)


### Features

* add components and store ([585953a](https://gitlab.com/mrastiak/yocale-code-assignment/commit/585953a25aa381983657e6b403f7cfe0de62be5c))
* add linters to project ([b77c8ec](https://gitlab.com/mrastiak/yocale-code-assignment/commit/b77c8ec09702765038c53f4748b844466448282d))
* add loading ([ca3c44c](https://gitlab.com/mrastiak/yocale-code-assignment/commit/ca3c44cbb7c38788ab85a61b1bdd03530a6ff6d6))
* add router to project ([3c0ac30](https://gitlab.com/mrastiak/yocale-code-assignment/commit/3c0ac302d485a795078de08045ec383dc094f8a6))
* add standard-version ([42f0b46](https://gitlab.com/mrastiak/yocale-code-assignment/commit/42f0b460b71917bc66465196c661dcea44fb2a05))
* working on components ([6a90792](https://gitlab.com/mrastiak/yocale-code-assignment/commit/6a90792afa5f8fe699bbdf70ce3dad657527563d))
* working on store ([e38c1cb](https://gitlab.com/mrastiak/yocale-code-assignment/commit/e38c1cb642916c2d3f3bc1d6513365b8b7a3f592))


### Bug Fixes

* stylelint and prettier conflict ([f5bc163](https://gitlab.com/mrastiak/yocale-code-assignment/commit/f5bc1631e7fc98ae146242e03547f7301908ac48))
