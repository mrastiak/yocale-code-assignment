import React, { Suspense } from "react";
import { Router as ReactRouter, Switch, Route } from "react-router-dom";
import routes from "router/routes";
import history from "utils/history";

function Router() {
  return (
    <ReactRouter history={history}>
      <Switch>
        {routes.map((route) => (
          <Suspense fallback={<div>Loading...</div>} key={route.path}>
            <Route {...route} />
          </Suspense>
        ))}
      </Switch>
    </ReactRouter>
  );
}

export default Router;
