import { configureStore } from "@reduxjs/toolkit";
import submarinesReducer from "app/store/reducers/submarines";
import torpedoesReducer from "app/store/reducers/torpedoes";

export const store = configureStore({
  reducer: {
    submarines: submarinesReducer,
    torpedoes: torpedoesReducer,
  },
});
