import React, { useMemo } from "react";
import PropTypes from "prop-types";

function Actions({ className, onLoad, onUnload, selectedSubmarine, loading }) {
  const loadingDisabled = useMemo(() => {
    return (
      !selectedSubmarine ||
      selectedSubmarine.equipments.torpedoes.length >= 3 ||
      loading
    );
  }, [selectedSubmarine, loading]);

  const unloadingDisabled = useMemo(() => {
    return (
      !selectedSubmarine ||
      selectedSubmarine.equipments.torpedoes.length <= 0 ||
      loading
    );
  }, [selectedSubmarine, loading]);

  return (
    <section className={className}>
      <button onClick={onLoad} disabled={loadingDisabled}>
        Load
      </button>
      <button onClick={onUnload} disabled={unloadingDisabled}>
        Unload
      </button>
      {loading && (
        <>
          <br />
          <p>Please wait</p>
        </>
      )}
    </section>
  );
}

Actions.propTypes = {
  className: PropTypes.string,
  onLoad: PropTypes.func.isRequired,
  onUnload: PropTypes.func.isRequired,
  selectedSubmarine: PropTypes.shape({
    equipments: PropTypes.shape({
      torpedoes: PropTypes.array,
    }),
  }),
  loading: PropTypes.bool.isRequired,
};

Actions.defaultProps = {
  className: "",
  selectedSubmarine: null,
};

export default Actions;
