import React from "react";
import PropTypes from "prop-types";
import Submarine from "components/Submarine";

function SubmarineList({ className, data, onSelect, selected }) {
  return (
    <section className={className}>
      {data.map((submarine) => (
        <Submarine
          key={submarine.id}
          data={submarine}
          onSelect={onSelect}
          selected={submarine.id === selected?.id}
        />
      ))}
    </section>
  );
}

SubmarineList.propTypes = {
  className: PropTypes.string,
  data: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    })
  ).isRequired,
  selected: PropTypes.shape({
    id: PropTypes.string,
  }),
  onSelect: PropTypes.func.isRequired,
};

SubmarineList.defaultProps = {
  className: "",
  selected: {},
};

export default SubmarineList;
